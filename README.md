# Restaurant POS

Guide to run application

Migrate Database:
- Cài đặt SQLServer Express 
- Tạo database "QLNhaHang" trong SQL Server
- Trong Visual Studio vào View > Other Windows > Package Manager Console
- Dưới Package Manager Console, chọn "Default Project" là "DAL", chạy lệnh console "Update-database"
- “Set as startup project” trong phần GUI

# Login

Admin:
- Username: admin
- Password: admin

Normal User:
- Username: user
- Password: user
